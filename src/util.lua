function mix(x, y, a)
  return x * (1 - a) + y * a
end

function math.sign(x)
  if x < 0 then return -1 end
  if x > 0 then return 1 end
  return 0
end