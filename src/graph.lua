local self = {}

local quality = 256
local graph = {}

self.touchtimer = 0
self.zoom = 1

local zoome = 1

local timer = 0

function self.update(dt)
  for i = 1, quality do
    local a = (i - 1) / (quality - 1)
    if not graph[i] then
      graph[i] = ease.ease(a)
    end
  end
  for i,v in ipairs(graph) do
    local a = (i - 1) / (quality - 1)
    local b = ease.ease(a)
    if minEase then
      b = b / 2 + 0.5
    end
    graph[i] = mix(v, b, math.min(dt * 18, 1))
    if graph[i] ~= graph[i] then -- is nan
      graph[i] = b
    end
  end
  self.touchtimer = mix(self.touchtimer, 0, dt * 2)

  -- lots of math just for the timer
  -- may need to abstract this out, somehow
  -- todo
  local sw, sh = love.graphics.getDimensions()
  local csize = 10 -- preview point size
  local size = math.min((sw - outerpadding) - ((dropdown.kget('ease2') or dropdown.kget('ease1')).x + dropdownWidth + padding), sh - outerpadding * 2 - padding * 3 - csize)
  local x, y, w, h = sw - outerpadding - size, outerpadding, size, size
  local mx, my = getMousePosition()

  if not (love.mouse.isDown(1) and mx > x and mx < x + w and my > y and my < y + h) then
    timer = (timer + dt * ((slider.kvalue('bpm') or 120)/120)) % 2
  else
    timer = mix(timer, (mx - x) / w, dt * 14)
  end

  zoome = mix(zoome, self.zoom, dt * 6)
end

function self.render()
  local sw, sh = love.graphics.getDimensions()

  if mode == modes.preview or mode == modes.mix or mode == modes.multiply then
    local csize = 10 -- preview point size
    local size = math.min((sw - outerpadding) - ((dropdown.kget('ease2') or dropdown.kget('ease1')).x + dropdownWidth + padding), sh - outerpadding * 2 - padding * 3 - csize)

    if screenshot then
      size = screenshotsize - outerpadding * 2
    end

    local x, y, w, h = sw - outerpadding - size, outerpadding, size, size
    if screenshot then
      x, y = outerpadding, outerpadding
    end

    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.rectangle('line', x, y, w, h)

    -- grid
    love.graphics.setColor(0.2, 0.2, 0.3, 0.2)
    local gridsize = 64
    for gx = 1, gridsize - 2 do
      love.graphics.line(x + margin + gx * w/gridsize, y + margin, x + margin + gx * w/gridsize, y + h - margin)
    end
    for gy = 1, gridsize - 2 do
      love.graphics.line(x + margin, y + margin + gy * h/gridsize, x + w - margin, y + margin + gy * h/gridsize)
    end

    -- mixease point
    if mode == modes.mix and slider.kget('mix') then
      love.graphics.setColor(1, 1, 1, 0.2 + self.touchtimer * 0.6)
      love.graphics.line(x + slider.kvalue('mix') * w, y, x + slider.kvalue('mix') * w, y + h)
    end

    -- preview point
    local t = timer % 1
    if not screenshot then
      love.graphics.setColor(0.4, 0.4, 1, 0.4)
      love.graphics.line(x + t * w, y, x + margin + t * (w - margin * 2), y + h)
    end

    -- y = 0 point
    -- todo: this will break with eases that dont have the first point at y0
    local py = graph[1]
    love.graphics.setColor(0.7, 0.7, 0.7, 0.4 * (1 - math.abs(py - 0.5) / 0.5))
    love.graphics.line(x, y + h - py * h, x + w, y + py * h)

    -- polygone
    -- this isnt done with a polygon because else itd waste a Bunch of ram and i kinda, dont want to do that?
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.setScissor(x + margin, y, math.abs(w - (margin * 2)), math.abs(h))
    local last = (((graph[1] or 0) - 0.5) * zoome) + 0.5
    for gx = 1, quality - 1 do
      local a = gx/quality
      local b = graph[gx + 1] or 0
      b = ((b - 0.5) * zoome) + 0.5
      local px, py = x + margin + gx * ((w - margin * 2)/quality), y + h - margin - b * (h - margin * 2)
      local ox, oy = x + margin + (gx - 1) * ((w - margin * 2)/quality), y + h - margin - last * (h - margin * 2)
      if math.abs(b - last) < 1 then
        love.graphics.line(ox, oy, px, py)
      end
      last = b
    end
    love.graphics.setScissor()

    -- zoom lines
    local a = math.min(math.abs(zoome - 1) * 10, 1)
    if a > 0 then
      love.graphics.setColor(1, 1, 1, a * 0.2)
      local ytop = y + h/2 - h * zoome * 0.5
      local ybot = y + h/2 + h * zoome * 0.5
      love.graphics.line(x, ytop, x + w, ytop)
      love.graphics.line(x, ybot, x + w, ybot)
      love.graphics.setColor(1, 1, 1, a * 0.3)
      love.graphics.print('y=1', x, ytop)
      love.graphics.print('y=0', x, ybot)
    end

    -- preview
    if not screenshot then
      love.graphics.setScissor(x - margin, 0, math.abs(w + margin * 2), sh)
      love.graphics.setColor(0.16, 0.16, 0.17, 1)
      love.graphics.line(x + margin, y + h + padding * 2 + csize/2, x + w - margin, y + h + padding * 2 + csize/2)
      love.graphics.setColor(0.3, 0.3, 0.31, 1)
      love.graphics.line(x + margin + (w - margin * 2) * (1 - zoome) * 0.5, y + h + padding * 2 + csize/2, x + w - margin - (w - margin * 2) * (1 - zoome) * 0.5, y + h + padding * 2 + csize/2)

      love.graphics.setColor(0.4, 0.4, 1, 1)
      local a1 = ease.ease(t)
      local a2 = ease.ease(math.max(math.min(t - 0.1, 1), 0))
      local da = ((a1 - 0.5) * zoome) + 0.5
      if timer % 2 > 1 and math.floor(ease.ease(0) + 0.5) ~= math.floor(ease.ease(1) + 0.5) then
        da = 1 - da
      end
      if minEase then
        da = da / 2 + 0.5
      end

      love.graphics.ellipse('fill', x + margin + (w - margin * 2) * da, y + h + padding * 2 + csize/2, csize * (1 + math.min(math.abs(a1 - a2), 3) * 1.2), csize)
      love.graphics.setScissor()
    end
  end
end

function self.wheelmoved(wx, wy)
  local mx, my = getMousePosition()

  -- may need to abstract this out, somehow
  -- todo
  local sw, sh = love.graphics.getDimensions()
  local csize = 10 -- preview point size
  local size = math.min((sw - outerpadding) - ((dropdown.kget('ease2') or dropdown.kget('ease1')).x + dropdownWidth + padding), sh - outerpadding * 2 - padding * 3 - csize)
  local x, y, w, h = sw - outerpadding - size, outerpadding, size, size

  if mx > x and mx < x + w and my > y and my < y + h then
    self.zoom = self.zoom - wy * 0.1
    self.zoom = math.min(self.zoom, 1)
    self.zoom = math.max(self.zoom, 0.1)
  end
end

return self