local self = {}

local easelib = require 'src.easelib'

function self.mixEase(e1, e2, point, param1, param2)
  if not point then point = 0.5 end

  return function(a)
    if a < point then
      return e1(a / point, param1[1], param1[2]) * point
    else
      return e2((a - point) / (1 - point), param2[1], param2[2]) * (1 - point) + point
    end
  end
end

self.eases = {}
for i,v in pairs(easelib) do
  local min = 0

  local params = {}
  for i = 3, #v do
    if v[i] then
      table.insert(params, {min = v[i][1], max = v[i][2], default = v[i][3], name = v[i][4]})
    end
  end

  local q = 10
  for i = 0, q do
    local s = v[2](i / q, (params[1] or {}).default, (params[2] or {}).default)
    if s < 0 and not v.overridemin then min = -1 end
  end

  self.eases[v[1]] = {
    f = v[2],
    max = 1,
    min = min,
    i = i,
    name = v[1],
    params = params,
    type = v.type
  }
end

self.ease = nil
self.minEase = false

return self