local self = {}

local tooltipwidth = 0
local tooltipx = 0
local tooltipy = 0

local tooltipopen = false
local tooltiptext = ''
local tooltiptargetwidth = 0

local tooltipframe = false

function self.update(dt)
  local mx, my = getMousePosition()
  tooltipx = mix(tooltipx, mx, dt * 18)
  tooltipy = mix(tooltipy, my, dt * 18)

  tooltipwidth = mix(tooltipwidth, tooltiptargetwidth, dt * 12)
end

function self.prerender()
  tooltipframe = false
end

function self.show(text)
  if love.mouse.isDown(1) or dropdown.openDropdown ~= 0 then return end
  tooltipframe = true
  tooltiptext = text
  tooltiptargetwidth = love.graphics.newText(love.graphics.getFont(), text):getWidth()
end

local function softlimit(x, f)
  local sign = math.sign(x)

  return sign * (f - (1.027 ^ (-10 * math.abs(x))) * f)
end

function self.render()
  local mx, my = getMousePosition()
  if not tooltipframe then
    tooltiptargetwidth = 0
  end

  if tooltipwidth > 1 then
    local a = math.min((tooltipwidth - 1) / 6, 1)

    local x, y, w, h = mx + 8, my + 8, (tooltipwidth + 4 + margin) * 0.8, (fontHeight + margin) * 0.8

    local easiness = 3 -- hehe. magic numbers
    local scale = 1
    local sx, sy = ((w - softlimit(mx - tooltipx, w/easiness)/easiness) / w) * scale, ((h - softlimit(my - tooltipy, h/easiness)/easiness) / h) * scale

    love.graphics.push()

    love.graphics.translate(x, y)
    love.graphics.scale(sx, sy)

    love.graphics.setColor(0.2, 0.2, 0.3, a)
    love.graphics.rectangle('fill', 0, 0, w, h)

    love.graphics.setScissor(0, 0, math.max(mx + (tooltipwidth + 2 + margin/2 + 16) * sx, 0), love.graphics.getHeight())

    love.graphics.setColor(1, 1, 1, a)
    love.graphics.setFont(getFont(0.8, false))
    love.graphics.print(tooltiptext, 2, 2)
    love.graphics.setFont(interfaceFont)

    love.graphics.setScissor()

    love.graphics.pop()
  end
end

return self