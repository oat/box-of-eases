local self = {}

local sliders = {}

function self.get(index)
  return sliders[index]
end

function self.value(index)
  return sliders[index].value
end

function self.kget(key)
  for _, v in ipairs(sliders) do
    if v.name == key then
      return v
    end
  end
end

function self.kvalue(key)
  return self.kget(key) and self.kget(key).oldvalue
end

local sliderId
local function insertSlider(tab, f)
  sliderId = sliderId + 1
  f.value = (self.kget(f.name) or {value = f.default}).value
  f.oldvalue = (self.kget(f.name) or {oldvalue = f.value}).oldvalue
  f.spintimer = (self.kget(f.name) or {spintimer = 0}).spintimer
  f.spin = (self.kget(f.name) or {spin = 0}).spin
  if f.snap then
    f.value = math.floor(f.value / f.snap) * f.snap
  end
  return table.insert(tab, f)
end
function self.createSliders()
  local s = {}
  sliderId = 0

  if mode == modes.mix then -- mix eases
    insertSlider(s, {
      x = outerpadding,
      y = outerpadding + fontHeight * 2.5 + padding,
      width = dropdownWidth,
      min = 0,
      max = 1,
      default = 0.5,
      name = 'mix',
      displayname = 'Mix',
      snap = 0.01,
      tooltip = 'The point at which the first ease snaps into the second one'
    })
  end
  if mode == modes.preview or mode == modes.mix or mode == modes.multiply then -- bpm slider
    insertSlider(s, {
      x = outerpadding,
      y = love.graphics.getHeight() - outerpadding - fontHeight * 3 - padding,
      width = dropdownWidth,
      min = 80,
      max = 220,
      default = 120,
      name = 'bpm',
      displayname = 'BPM',
      snap = 1,
      tooltip = 'The speed of the preview dot in Beats Per Minute'
    })
  end

  local ease1 = ease.eases[dropdown.kselected('ease1')]
  local ease2 = ease.eases[dropdown.kselected('ease2')]

  local param1 = ease1 and ease1.params
  local param2 = ease2 and ease2.params

  if param1 then
    for i,v in ipairs(param1) do
      insertSlider(s, {
        x = outerpadding + dropdownWidth + padding,
        y = outerpadding + (fontHeight * 3 + padding) * i - padding,
        width = dropdownWidth,
        min = v.min,
        max = v.max,
        default = v.default,
        name = ease1.name .. 'param1' .. i,
        topdisplayname = 'Parameter',
        displayname = v.name
      })
    end
  end
  if param2 then
    for i,v in ipairs(param2) do
      insertSlider(s, {
        x = outerpadding + dropdownWidth + padding + dropdownWidth + padding,
        y = outerpadding + (fontHeight * 3 + padding) * i - padding,
        width = dropdownWidth,
        min = v.min,
        max = v.max,
        default = v.default,
        name = ease2.name .. 'param2' .. i,
        topdisplayname = 'Parameter',
        displayname = v.name
      })
    end
  end

  sliders = s
end

local function normalize(a, min, max)
  return (a - min) / (max - min)
end

function self.update(dt)
  for i, v in ipairs(sliders) do
    v.spintimer = mix(v.spintimer + math.abs(normalize(v.value, v.min, v.max) - normalize(v.oldvalue, v.min, v.max)) * dt * 40, 0, math.min(dt * 12, 1))
    v.oldvalue = mix(v.oldvalue, v.value, math.min(dt * 20, 1))

    if v.spintimer > 1 then
      v.spintimer = v.spintimer - 1
      v.spin = v.spin + 4
    end

    v.spin = mix(v.spin, 0, dt * 3)
  end
end

function self.render()
  local mx, my = getMousePosition()

  for i, v in ipairs(sliders) do
    local x, y, w, h = v.x, v.y, v.width, fontHeight * 1.25

    love.graphics.setColor(0.7, 0.7, 0.7, 0.4)
    love.graphics.line(x, y + h/2, x + w, y + h/2)

    local normalvalue = normalize(v.value, v.min, v.max)
    local normaloldvalue = normalize(v.oldvalue, v.min, v.max)
    local normaldefault = normalize(v.default, v.min, v.max)

    love.graphics.setColor(0.7, 0.7, 0.7, 0.3)
    love.graphics.line(x + normaldefault * w, y, x + normaldefault * w, y + h)

    local sx, sy = x + w * normaloldvalue, y + h/2
    local ssize = h * 0.75

    love.graphics.push()

    love.graphics.translate(sx, sy)
    love.graphics.rotate((normalvalue - normaloldvalue) * 4 + v.spin * math.pi * 2)

    local hovering = mx > sx - ssize/2 and mx < sx + ssize/2 and my > sy - ssize/2 and my < sy + ssize/2 and dropdown.openDropdown == 0
    local dragging = mx > x and mx < x + w and my > y and my < y + h and love.mouse.isDown(1) and dropdown.openDropdown == 0

    love.graphics.setColor(0, 0, 0, 1)
    if hovering or dragging then
      love.graphics.setColor(0.2, 0.2, 0.3, 1)
      if v.tooltip then tooltips.show(v.tooltip) end
    end
    love.graphics.rectangle('fill', -ssize/2, -ssize/2, ssize, ssize)
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.rectangle('line', -ssize/2, -ssize/2, ssize, ssize)

    love.graphics.rotate((normalvalue - normaloldvalue) * -2)

    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.printf(math.floor(v.value * 100)/100, -ssize * 6, ssize - 2, ssize * 12, 'center')

    love.graphics.pop()

    love.graphics.printf(v.displayname, v.x + margin * 2 - ssize * 6, v.y - ssize, ssize * 12, 'center')

    if v.topdisplayname then
      love.graphics.setColor(0.4, 0.4, 0.55, 1)
      love.graphics.setFont(getFont(0.75, true))
      love.graphics.printf(v.topdisplayname, v.x - ssize * 6, v.y - ssize * 1.45, ssize * 12, 'center')
      love.graphics.setFont(interfaceFont)
    end

    if dragging then
      v.value = ((mx - (x + 1)) / (w - 2)) * (v.max - v.min) + v.min
      if v.name == 'mix' then graph.touchtimer = 1 end -- sorry !!!
      createUI()
    end
    if mx > x and mx < x + w and my > y and my < y + h and love.mouse.isDown(2) and dropdown.openDropdown == 0 then
      v.value = v.default
      createUI()
    end
    if mx > x and mx < x + w and my > y and my < y + h and love.mouse.isDown(3) and dropdown.openDropdown == 0 then
      v.value = v.min + math.random() * (v.max - v.min)
      createUI()
    end
  end
end

return self