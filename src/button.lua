local self = {}

local icon = {
  clipboard = love.graphics.newImage('assets/textures/clipboard.png'),
  screenshot = love.graphics.newImage('assets/textures/screenshot.png'),
  swap = love.graphics.newImage('assets/textures/swap.png'),
}

local buttons = {}

function self.get(index)
  return buttons[index]
end

function self.kget(key)
  for _, v in ipairs(buttons) do
    if v.name == key then
      return v
    end
  end
end

local function nillify(tab) -- dumb hack. turns `false` into `nil`
  local t = {}
  for i,v in ipairs(tab) do
    if v then table.insert(t, v) end
  end
  return t
end

local buttonId
local function insertButton(tab, f)
  buttonId = buttonId + 1
  f.press = (self.kget(f.name) or {press = 1}).press
  return table.insert(tab, f)
end
function self.createButtons()
  local s = {}
  buttonId = 0

  if mode == modes.mix or mode == modes.preview or mode == modes.multiply then
    insertButton(s, {
      x = outerpadding,
      y = love.graphics.getHeight() - outerpadding - fontHeight * 4 - padding * 2 - 32,
      size = 32,
      name = 'clipboard',
      displayname = 'Copy to Clipboard',
      tooltip = 'Copy to Clipboard',
      icon = 'clipboard',
      func = function()
        local s = ''

        local param1 = {}
        local param2 = {}

        if mode == modes.preview then
          local e = ease.eases[dropdown.kselected('ease1')]

          param1[1] = slider.kvalue(e.name .. 'param11')
          param1[2] = slider.kvalue(e.name .. 'param12')

          local p1 = ''
          if param1[1] ~= (e.params[1] and e.params[1].default) or param1[2] ~= (e.params[2] and e.params[2].default) then
            for i,v in ipairs(param1) do
              v = math.floor(v * 100) / 100
              p1 = p1 .. (i > 1 and (', ' .. v) or v)
            end
          end

          s = e.name .. (p1 ~= '' and ('.params(' .. p1 .. ')') or '')
        elseif mode == modes.mix then
          local e1 = ease.eases[dropdown.kselected('ease1')]
          local e2 = ease.eases[dropdown.kselected('ease2')]

          param1[1] = slider.kvalue(e1.name .. 'param11')
          param1[2] = slider.kvalue(e1.name .. 'param12')
          param2[1] = slider.kvalue(e2.name .. 'param11')
          param2[2] = slider.kvalue(e2.name .. 'param12')

          local p1 = ''
          if param1[1] ~= (e1.params[1] and e1.params[1].default) or param1[2] ~= (e1.params[2] and e1.params[2].default) then
            for i,v in ipairs(param1) do
              v = math.floor(v * 100) / 100
              p1 = p1 .. (i > 1 and (', ' .. v) or v)
            end
          end
          local p2 = ''
          if param2[1] ~= (e2.params[1] and e2.params[1].default) or param2[2] ~= (e2.params[2] and e2.params[2].default) then
            for i,v in ipairs(param2) do
              v = math.floor(v * 100) / 100
              p2 = p2 .. (i > 1 and (', ' .. v) or v)
            end
          end

          s = 'mixEase(' .. e1.name .. (p1 ~= '' and ('.params(' .. p1 .. ')') or '') .. ', ' .. e2.name .. (p2 ~= '' and ('.params(' .. p2 .. ')') or '') .. ', ' .. (math.floor(slider.kvalue('mix') * 100) / 100) .. ')'
        elseif mode == modes.multiply then
          local e1 = ease.eases[dropdown.kselected('ease1')]
          local e2 = ease.eases[dropdown.kselected('ease2')]

          param1[1] = slider.kvalue(e1.name .. 'param11')
          param1[2] = slider.kvalue(e1.name .. 'param12')
          param2[1] = slider.kvalue(e2.name .. 'param11')
          param2[2] = slider.kvalue(e2.name .. 'param12')

          local p1 = ''
          if param1[1] ~= (e1.params[1] and e1.params[1].default) or param1[2] ~= (e1.params[2] and e1.params[2].default) then
            for i,v in ipairs(param1) do
              v = math.floor(v * 100) / 100
              p1 = p1 .. (i > 1 and (', ' .. v) or v)
            end
          end
          local p2 = ''
          if param2[1] ~= (e2.params[1] and e2.params[1].default) or param2[2] ~= (e2.params[2] and e2.params[2].default) then
            for i,v in ipairs(param2) do
              v = math.floor(v * 100) / 100
              p2 = p2 .. (i > 1 and (', ' .. v) or v)
            end
          end

          s = 'function(x) ' .. e2.name .. (p2 ~= '' and ('.params(' .. p2 .. ')') or '') .. '(' .. e1.name .. (p1 ~= '' and ('.params(' .. p1 .. ')') or '') .. '(x)) end'
        end
        love.system.setClipboardText(s)
      end
    })
    insertButton(s, {
      x = outerpadding + padding + 32,
      y = love.graphics.getHeight() - outerpadding - fontHeight * 4 - padding * 2 - 32,
      size = 32,
      name = 'screenshot',
      displayname = 'Screenshot',
      tooltip = 'Take a screenshot',
      icon = 'screenshot',
      func = function()
        screenshot = true
        screenshotCanvas = love.graphics.newCanvas()
      end
    })
  end

  if mode == modes.mix or mode == modes.multiply then
    insertButton(s, {
      x = outerpadding + dropdownWidth * 2 + padding,
      y = outerpadding + fontHeight/2 - padding/2,
      size = padding,
      name = 'swap',
      displayname = 'Swap',
      icon = 'swap',
      func = function()
        dropdown.swap('ease1', 'ease2')
        createUI()
      end
    })
  end

  buttons = s
end

function self.update(dt)
  for i, v in ipairs(buttons) do
    local mx, my = getMousePosition()

    local targetsize = 1
    if mx > v.x and mx < v.x + v.size and my > v.y and my < v.y + v.size and dropdown.openDropdown == 0 then
      if love.mouse.isDown(1) then
        targetsize = 0.8
      else
        targetsize = 0.95
      end
    end

    v.press = mix(v.press, targetsize, dt * 12)
  end
end

function self.render()
  local mx, my = getMousePosition()

  for i, v in ipairs(buttons) do
    local x, y, w, h = v.x, v.y, v.size, v.size

    w = w * v.press
    h = h * v.press
    x = x + (v.size - w) / 2
    y = y + (v.size - h) / 2

    local hovering = mx > x and mx < x + w and my > y and my < y + h and dropdown.openDropdown == 0
    local clicking = hovering and love.mouse.isDown(1)

    love.graphics.setColor(0, 0, 0, 1)
    if hovering or dragging then
      love.graphics.setColor(0.4, 0.4, 1, 1)
      if v.tooltip then tooltips.show(v.tooltip) end
    end
    love.graphics.rectangle('fill', x, y, w, h)
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.rectangle('line', x, y, w, h)

    if v.icon and icon[v.icon] then
      local icon = icon[v.icon]
      love.graphics.draw(icon, x, y, 0, w / icon:getWidth(), h / icon:getHeight())
    end
  end
end

function self.mousepressed(x, y, m)
  for i, v in ipairs(buttons) do
    if x > v.x and x < v.x + v.size and y > v.y and y < v.y + v.size and m == 1 then
      if v.func then v.func() end
    end
  end
end

return self