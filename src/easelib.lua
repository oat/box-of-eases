local self = {}

local sqrt = math.sqrt
local sin = math.sin
local asin = math.asin
local cos = math.cos
local pow = math.pow
local exp = math.exp
local pi = math.pi
local abs = math.abs

self = setmetatable(self, {
  __index = function(s, i)
    for _,v in ipairs(self) do
      if v[1] == i then
        return v[2]
      end
    end
  end
})

table.insert(self, {'linear', function(t) return t end, type = 'linear'})
table.insert(self, {'instant', function() return 1 end, type = 'instant'})

table.insert(self, {'bounce', function(t) return 4 * t * (1 - t) end, type = 'transient'})
table.insert(self, {'tri', function(t) return 1 - abs(2 * t - 1) end, type = 'transient'})
table.insert(self, {'bell', function(t) return self.inOutQuint(self.tri(t)) end, type = 'transient'})
table.insert(self, {'pop', function(t) return 3.5 * (1 - t) * (1 - t) * sqrt(t) end, type = 'transient'})
table.insert(self, {'tap', function(t) return 3.5 * t * t * sqrt(1 - t) end, type = 'transient'})
table.insert(self, {'pulse', function(t) return t < .5 and self.tap(t * 2) or -self.pop(t * 2 - 1) end, type = 'pulse'})

table.insert(self, {'spike', function(t) return exp(-10 * abs(2 * t - 1)) end, type = 'transient'})
table.insert(self, {'inverse', function(t) return t * t * (1 - t) * (1 - t) / (0.5 - t) end, type = 'pulse'})

table.insert(self, {'popElastic', function(t, damp, count)
  return (1000 ^ -(t ^ damp) - 0.001) * sin(count * pi * t)
end, {0, 10, 1.4, 'damp'}, {1, 25, 6, 'count'}, type = 'pulse'})
table.insert(self, {'tapElastic', function(t, damp, count)
  return (1000 ^ -((1 - t) ^ damp) - 0.001) * sin(count * pi * (1 - t))
end, {0, 10, 1.4, 'damp'}, {1, 25, 6, 'count'}, type = 'pulse'})
table.insert(self, {'pulseElastic', function(t, damp, count)
  if t < .5 then
    return self.tapElastic(t * 2, damp, count)
  else
    return -self.popElastic(t * 2 - 1, damp, count)
  end
end, {0, 10, 1.4, 'damp'}, {1, 25, 6, 'count'}, type = 'pulse'})

table.insert(self, {'impulse', function(t, damp)
  t = t ^ damp
  return t * (1000 ^ -t - 0.001) * 18.6
end, {0, 10, 0.9, 'damp'}, type = 'transient'})

table.insert(self, {'inSine', function(x)
  return 1 - cos(x * (pi * 0.5))
end, type = 'in'})

table.insert(self, {'outSine', function(x)
  return sin(x * (pi * 0.5))
end, type = 'out'})

table.insert(self, {'inOutSine', function(x)
  return 0.5 - 0.5 * cos(x * pi)
end, type = 'inout'})

table.insert(self, {'inQuad', function(t) return t * t end, type = 'in'})
table.insert(self, {'outQuad', function(t) return -t * (t - 2) end, type = 'out'})
table.insert(self, {'inOutQuad', function(t)
  t = t * 2
  if t < 1 then
    return 0.5 * t ^ 2
  else
    return 1 - 0.5 * (2 - t) ^ 2
  end
end, type = 'inout'})
table.insert(self, {'inCubic', function(t) return t * t * t end, type = 'in'})
table.insert(self, {'outCubic', function(t) return 1 - (1 - t) ^ 3 end, type = 'out'})
table.insert(self, {'inOutCubic', function(t)
  t = t * 2
  if t < 1 then
    return 0.5 * t ^ 3
  else
    return 1 - 0.5 * (2 - t) ^ 3
  end
end, type = 'inout'})
table.insert(self, {'inQuart', function(t) return t * t * t * t end, type = 'in'})
table.insert(self, {'outQuart', function(t) return 1 - (1 - t) ^ 4 end, type = 'out'})
table.insert(self, {'inOutQuart', function(t)
  t = t * 2
  if t < 1 then
    return 0.5 * t ^ 4
  else
    return 1 - 0.5 * (2 - t) ^ 4
  end
end, type = 'inout'})
table.insert(self, {'inQuint', function(t) return t ^ 5 end, type = 'in'})
table.insert(self, {'outQuint', function(t) return 1 - (1 - t) ^ 5 end, type = 'out'})
table.insert(self, {'inOutQuint', function(t)
  t = t * 2
  if t < 1 then
    return 0.5 * t ^ 5
  else
    return 1 - 0.5 * (2 - t) ^ 5
  end
end, type = 'inout'})
table.insert(self, {'inExpo', function(t) return 1000 ^ (t - 1) - 0.001 end, type = 'in'})
table.insert(self, {'outExpo', function(t) return 1.001 - 1000 ^ -t end, type = 'out'})
table.insert(self, {'inOutExpo', function(t)
  t = t * 2
  if t < 1 then
    return 0.5 * 1000 ^ (t - 1) - 0.0005
  else
    return 1.0005 - 0.5 * 1000 ^ (1 - t)
  end
end, type = 'inout'})
table.insert(self, {'inCirc', function(t) return 1 - sqrt(1 - t * t) end, type = 'in'})
table.insert(self, {'outCirc', function(t) return sqrt(-t * t + 2 * t) end, type = 'out'})
table.insert(self, {'inOutCirc', function(t)
  t = t * 2
  if t < 1 then
    return 0.5 - 0.5 * sqrt(1 - t * t)
  else
    t = t - 2
    return 0.5 + 0.5 * sqrt(1 - t * t)
  end
end, type = 'inout'})

table.insert(self, {'inBounce', function(t) return 1 - self.outBounce(1 - t) end, type = 'in'})
table.insert(self, {'outBounce', function(t)
  if t < 1 / 2.75 then
    return 7.5625 * t * t
  elseif t < 2 / 2.75 then
    t = t - 1.5 / 2.75
    return 7.5625 * t * t + 0.75
  elseif t < 2.5 / 2.75 then
    t = t - 2.25 / 2.75
    return 7.5625 * t * t + 0.9375
  else
    t = t - 2.625 / 2.75
    return 7.5625 * t * t + 0.984375
  end
end, type = 'out'})
table.insert(self, {'inOutBounce', function(t)
  if t < 0.5 then
    return self.inBounce(t * 2) * 0.5
  else
    return self.outBounce(t * 2 - 1) * 0.5 + 0.5
  end
end, type = 'inout'})

table.insert(self, {'inElastic', function(t, a, p)
  return 1 - self.outElastic(1 - t, a, p)
end, {1, 3, 1, 'a'}, {0, 2, 0.3, 'p'}, overridemin = true, type = 'in'})
table.insert(self, {'outElastic', function(t, a, p)
  return a * pow(2, -10 * t) * sin((t - p / (2 * pi) * asin(1/a)) * 2 * pi / p) + 1
end, {1, 3, 1, 'a'}, {0, 2, 0.3, 'p'}, overridemin = true, type = 'out'})
table.insert(self, {'inOutElastic', function(t, a, p)
  return t < 0.5
    and  0.5 * self.inElastic(t * 2, a, p)
    or  0.5 + 0.5 * self.outElastic(t * 2 - 1, a, p)
end, {1, 3, 1, 'a'}, {0, 2, 0.3, 'p'}, overridemin = true, type = 'inout'})

table.insert(self, {'inBack', function(t, a)
  return t * t * (a * t + t - a)
end, {0, 3, 1.70158, 'a'}, overridemin = true, type = 'in'})
table.insert(self, {'outBack', function(t, a)
  t = t - 1 return t * t * ((a + 1) * t + a) + 1
end, {0, 3, 1.70158, 'a'}, overridemin = true, type = 'out'})
table.insert(self, {'inOutBack', function(t, a)
  return t < 0.5
  and  0.5 * self.inBack(t * 2, a)
  or  0.5 + 0.5 * self.outBack(t * 2 - 1, a)
end, {0, 3, 1.70158, 'a'}, overridemin = true, type = 'inout'})

-- custom stuff

local function exposin(x, e)
  if ((x - 1) % 4) < 2 then
    return math.pow(math.abs(x % 4 - 2), e) - 1
  else
    return -math.pow(math.abs((x + 2) % 4 - 2), e) + 1
  end
end
local function expocos(x, e)
  return exposin(x + 1, e)
end

table.insert(self, {'inExponent*', function(x, e)
  return 1 - -expocos(1 - x, e)
end, {0, 10, 2, 'e'}, type = 'in'})
table.insert(self, {'outExponent*', function(x, e)
  return -expocos(x, e)
end, {0, 10, 2, 'e'}, type = 'out'})
table.insert(self, {'inOutExponent*', function(x, e)
  return 0.5 - 0.5 * exposin(x * 2, e)
end, {0, 10, 2, 'e'}, type = 'inout'})

return self
