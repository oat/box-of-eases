local default_G = {}
for k, v in pairs(_G) do
  table.insert(default_G, k)
end

love.graphics.setDefaultFilter('nearest', 'nearest')

local fontSize = 20
interfaceFont = love.graphics.newFont('assets/fonts/Inter-Regular.otf', fontSize)

local fontCache = {}

function getFont(size, italic)
  if fontCache[size] and fontCache[size][italic] then return fontCache[size][italic] end

  fontCache[size] = fontCache[size] or {}
  fontCache[size][italic] = love.graphics.newFont('assets/fonts/Inter-' .. (italic and 'Italic' or 'Regular') .. '.otf', math.floor(fontSize * size))
  return fontCache[size][italic]
end

function getMousePosition()
  local x, y = love.mouse.getPosition()

  local sx, sy, sw, sh = 0, 0, love.graphics.getWidth(), love.graphics.getHeight()
  if love.isVersionCompatible('11.3') then sx, sy, sw, sh = love.window.getSafeArea() end

  x, y = ((x - sx) / sw) * love.graphics.getWidth(), ((y - sy) / sh) * love.graphics.getHeight()

  return x, y
end

function getMouseX()
  local x, y = getMousePosition()
  return x
end

function getMouseY()
  local x, y = getMousePosition()
  return y
end

ease = require 'src.ease'

slider = require 'src.slider'
dropdown = require 'src.dropdown'
graph = require 'src.graph'
button = require 'src.button'
tooltips = require 'src.tooltips'

modes = {
  preview = 1,
  mix = 2,
  multiply = 3,
  create = 4
}

function createUI()
  dropdown.createDropdowns()
  slider.createSliders()
  button.createButtons()
end

require 'src.util' -- exports into global table

-- rendering constants

padding = 16
outerpadding = 22
margin = 6
dropdownWidth = 186
lineWidth = 2
fontHeight = love.graphics.getFont():getHeight()

screenshot = false
screenshotCanvas = nil
screenshotsize = 512

mobile = love.system.getOS() == 'iOS' or love.system.getOS() == 'Android'

-- global for convinience's sake

mode = nil

function love.load()
  love.graphics.setFont(interfaceFont)
  fontHeight = love.graphics.getFont():getHeight()
  createUI()

  if mobile then
    love.window.setMode(640, 360, {borderless = true, resizable = false, minwidth = 705, minheight = 510, fullscreen = true})
  end
end

function love.update(dt)
  graph.update(dt)
  slider.update(dt)
  dropdown.update(dt)
  button.update(dt)
  tooltips.update(dt)
end

function love.draw()
  local sx, sy, sw, sh = 0, 0, love.graphics.getWidth(), love.graphics.getHeight()
  if love.isVersionCompatible('11.3') then sx, sy, sw, sh = love.window.getSafeArea() end

  local mx, my = love.mouse.getPosition()

  if screenshot then
    love.graphics.setCanvas(screenshotCanvas)
    sw, sh = screenshotsize, screenshotsize
    love.graphics.setColor(0.09, 0.09, 0.12, 1)
    love.graphics.rectangle('fill', 0, 0, sw, sh)
    love.graphics.setColor(0.08, 0.08, 0.1, 1)
    love.graphics.rectangle('line', 0, 0, sw, sh)
    graph.render()
    love.graphics.setCanvas()

    --[[
    -- for seeing what gets drawn to it
    love.graphics.setColor(1, 1, 1)
    love.graphics.draw(screenshotCanvas)

    return
    ]]

    local ss = screenshotCanvas:newImageData(0, 1, 0, 0, screenshotsize, screenshotsize)
    ss:encode('png', 'screenshot ' .. os.date('%Y-%m-%d %H-%M') .. '.png')
    love.system.openURL('file://' .. love.filesystem.getSaveDirectory())

    screenshot = false
  end

  love.graphics.setCanvas()

  -- this is fine to do since all textures are already loaded with nearest
  love.graphics.setDefaultFilter('linear', 'linear')

  love.graphics.setLineWidth(2)

  love.graphics.setColor(0.09, 0.09, 0.12, 1)
  love.graphics.rectangle('fill', 0, 0, love.graphics.getWidth(), love.graphics.getHeight())
  love.graphics.setColor(0.08, 0.08, 0.1, 1)
  love.graphics.rectangle('line', 0, 0, love.graphics.getWidth(), love.graphics.getHeight())

  love.graphics.push()
  love.graphics.translate(sx, sy)
  love.graphics.scale(sw/love.graphics.getWidth(), sh/love.graphics.getHeight())
  
  sw, sh = love.graphics.getDimensions()

  love.graphics.setColor(0.2, 0.2, 0.3, 1)
  love.graphics.print('Box of Eases by oatmealine', outerpadding, sh - fontHeight - outerpadding)

  tooltips.prerender()

  button.render()

  slider.render()

  dropdown.render()

  graph.render()

  tooltips.render()

  love.graphics.pop()
end

function love.mousepressed(x, y, m)
  local sx, sy, sw, sh = 0, 0, love.graphics.getWidth(), love.graphics.getHeight()
  if love.isVersionCompatible('11.3') then sx, sy, sw, sh = love.window.getSafeArea() end

  x, y = ((x - sx) / sw) * love.graphics.getWidth(), ((y - sy) / sh) * love.graphics.getHeight()

  if dropdown.mousepressed(x, y, m) then return end
  button.mousepressed(x, y, m)
end

function love.mousereleased(x, y, m)
  local sx, sy, sw, sh = 0, 0, love.graphics.getWidth(), love.graphics.getHeight()
  if love.isVersionCompatible('11.3') then sx, sy, sw, sh = love.window.getSafeArea() end

  x, y = ((x - sx) / sw) * love.graphics.getWidth(), ((y - sy) / sh) * love.graphics.getHeight()

  if dropdown.mousereleased(x, y, m) then return end
end

function love.wheelmoved(x, y)
  if y == 0 then return end

  if dropdown.wheelmoved(x, y) then return end
  graph.wheelmoved(x, y)
end

function love.keypressed(key)
  if key == 'f6' then -- print all globals
    for k, v in pairs(_G) do
      for _, g in ipairs(default_G) do
        if g == k then goto continue end
      end
      print(k, v)
      ::continue::
    end
  elseif key == 'f2' then -- mobile mode
    mobile = not mobile
  end
end

function love.resize()
  createUI()
end
