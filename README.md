# Box of Eases (working title)

![logo](./docs/logo.png)

A simple Love2D application to create, preview and mix eases, meant for NITG modding.

## How-to Install

### Manual/Development (currently the only method)

Download Love2D for your operating system here: https://love2d.org/

Then launch the game like so: https://love2d.org/wiki/Getting_Started#Running_Games

## Screenshots

![Previewing eases](./docs/screenshot1.png)

![Mixing eases](./docs/screenshot2.png)

## Attributions

Inter V Font: by The Inter Project Authors