function love.conf(t)
  t.identity = 'box-of-eases'
  t.version = '11.3'

  t.window.width = 1080
  t.window.height = 800
  t.window.resizable = true
  t.window.title = 'Box of Eases'
  t.window.icon = 'assets/textures/logo.png'
  t.window.msaa = 5
  t.window.vsync = false

  t.modules.audio = false
  t.modules.data = false
  t.modules.joystick = false
  t.modules.math = false
  t.modules.physics = false
  t.modules.sound = false
  t.modules.thread = false
  t.modules.video = false
end
